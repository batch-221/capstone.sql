#1 return the customerName of the customers who are from the Philippines

	SELECT `customerName` FROM Customers
WHERE Country='Philippines';

#2 return the contactLastName and contactFirstName of customers with name "La rochelle Gifts"

SELECT `contactLastName`, `contactFirstName` FROM Customers
WHERE CustomerName='La Rochelle Gifts';

#3 return the product name and MSRP of the product named "the titanic"

select `productName`, `MSRP` FROM products where productName='The Titanic';

#4 return the first and last name of employee whose email is "jfirrelli@classicmodelscars.com"

select `lastName`, `firstName` from employees where email='jfirrelli@classicmodelscars.com';

#5 return the names of customers who have no registered state

select `customerName` from Customers where state is NULL;

#6 return the first name, last name, email of the employee whose last name is Patterson and first name is steve

select `firstName`, `lastName`, `email` from employees where firstName='steve' and lastName='Patterson';

#7 return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater that 3000

select `customerName`, `country`, `creditLimit` from Customers where NOT country='USA' and creditLimit > 3000;

#8 return the customer numbers of orders whose comments contain the string 'DHL'

select `customerNumber` from orders where comments like '% DHL %';

#9 return the product lines whose text description mentions the phrase 'state of the art'

select `productLine` from productlines where textDescription like '% state of the art %';

#10 return the countries of customers without duplication
	select DISTINCT country from Customers;

#11 return the statuses of orders without duplication
	select DISTINCT status from orders;

#12 return the customer names and countries of customers whose country is USA, france, or Canada

select `customerName`, `country` from customers where country IN ('USA', 'France', 'Canada');


#13 return the first name, last name, and office's city of employees whose offices are in Tokyo

select `firstName`, `lastName`, `city` from employees INNER JOIN offices on offices.officeCode = employees.officeCode where city='Tokyo';


#14 return the customer names of customers who were served by the employee name "Leslie Thompson"


select `customerName` from customers JOIN employees on customers.salesRepEmployeeNumber = employees.employeeNumber where lastName='Thompson' AND firstName='Leslie';

#15 return the product names and customer name of products ordered by "Baane Mini Imports"
#	4 tables to be join


#16 return the employees' first names, employees' last names, customers names, and offices' countries of transaction whose customers and offices are in the same country # 3 tables to be join

select `firstName`, `lastName`, `customerName`, `offices` from employees INNER JOIN customers on employees.employeeNumber = customers.customerNumber INNER join offices on customers.customerNumber = offices.officeCode where 

#17 return the product name and quantity in stock of produtcs that belong to the product line "planes" with stock quantities less than 1000

select `productName`, `quantityInStock` from products where  productLine IN ('planes') AND quantityInStock < 1000;

#18 return the customer's name with a phone number containing "+81"

select `customerName` from customers where phone like '%+81%';